#include <SDL2/SDL.h>
#include <assert.h>

#define WIDTH 28
#define HEIGHT 31
#define PAWNS_SIZE 5

enum {PACMAN=0,RED=1,PINK=2,CYAN=3,ORANGE=4} pawn_type;

typedef struct {
	enum {DOT = 3, LINE = 5, L = 7, FILL = 8} type;
	enum {LEFT_DOWN = 3, LEFT_UP = 0, RIGHT_UP = 1, RIGHT_DOWN = 2} orientation;

} tile;

typedef struct {
	unsigned char data[WIDTH][HEIGHT];
	tile graphic[WIDTH][HEIGHT];
} map;

typedef struct {
	int x;
	int y;
} point;

typedef struct {
	int x;
	int y;
	enum {NONE=-1,LEFT=3,UP=0,RIGHT=1,DOWN=2} move;
	int old_move;
} pawn;



const point offset_neighbors[4] = {{0,-1},{1,0},{0,1},{-1,0}};
const point offset_diagnal[4] = {{-1, -1}, {1,-1},{1,1},{-1,1}};

int save_map(const char *filepath, map *m)
{
	FILE *fp = fopen(filepath, "w");
	if(!fp) return 0;
	for(int i=0;i<WIDTH;++i) {
		for(int j=0;j<HEIGHT;++j) {
			fprintf(fp, "%d,", m->data[i][j]);

		}
		fprintf(fp, "\n");
	}
	fclose(fp);
}

int load_map(const char *filepath, map *m)
{
	FILE *fp = fopen(filepath, "r");
	if(!fp) return 0;
	for(int i=0;i<WIDTH;++i) {
		for(int j=0;j<HEIGHT;++j) {
			fscanf(fp,"%hhd,",&m->data[i][j]);
		}
		fgetc(fp);
	}
	fclose(fp);

}

void generate_graphic(map *m)
{
	for(int i=0;i<WIDTH;++i) for (int j=0;j<HEIGHT;++j) {
		if(m->data[i][j] != 1) continue;
		int count_neighbors=0;
		for(int k=0;k<4;++k) {
			const int x = i + offset_neighbors[k].x, y = j + offset_neighbors[k].y;
			if((x < 0) || (x> WIDTH-1) || (y < 0) || (y > HEIGHT-1)) continue;
			if(m->data[x][y]) ++count_neighbors;
		}
		for(int k=0;k<4;++k) {
			const int x = i + offset_diagnal[k].x, y = j + offset_diagnal[k].y;
			if((x < 0) || (x> WIDTH-1) || (y < 0) || (y > HEIGHT-1)) continue;
			if(m->data[x][y]) ++count_neighbors;
		}


		m->graphic[i][j].type = count_neighbors==6?5:count_neighbors;
		switch(m->graphic[i][j].type) {
		case L: for(int k=0;k<4;++k) {
				if(!m->data[i+offset_diagnal[k].x][j+offset_diagnal[k].y]) m->graphic[i][j].orientation = (k+2)%4;
			}break;
		case LINE: for(int k=0;k<4;++k) {
				  if(!m->data[i + offset_neighbors[k].x][j + offset_neighbors[k].y]) m->graphic[i][j].orientation = (k+3)%4;
			  }break;
		case DOT: for(int k=0;k<4;++k) {
				if(m->data[i+offset_diagnal[k].x][j+offset_diagnal[k].y]) m->graphic[i][j].orientation = k;

			  }break;
			  

		}
//		printf("(%d, %d) - orientation was set to %d\n", i, j, m->graphic[i][j].orientation);


	}
}

void draw_tile(SDL_Renderer *ren, const tile * const t, int x, int y)
{
	const unsigned char lonermatrix[3][3] = {{0,0,0},{0,1,0},{0,0,0}};
	const unsigned char lmatrix[3][3] ={{1,1,1},{1,0,0},{1,0,0}};
	const unsigned char dotmatrix[3][3] ={{1,0,0},{0,0,0},{0,0,0}};
	const unsigned char linematrix[3][3] ={{1,1,1},{0,0,0},{0,0,0}};
	const unsigned char fillmatrix[3][3] ={{1,1,1},{1,1,1},{1,1,1}};
	const unsigned char (*matrix)[3][3];

	switch(t->type) {
		case L: matrix = &lmatrix;break;
		case DOT: matrix = &dotmatrix;break;
		case LINE: matrix = &linematrix;break;
		case FILL: matrix = &fillmatrix;break;
		default: matrix = &lonermatrix;break;

	}
	for(int i=0;i<3;++i) for(int j=0;j<3;++j) {
		int mx, my;
		switch(t->orientation) {
		case LEFT_UP: mx = i; my = j;
			break;
		case RIGHT_UP: mx = 2 - j; my =  i;
			break;
		case RIGHT_DOWN: mx = 2 - i; my = 2 - j;
			break;
		case LEFT_DOWN: mx = j; my = 2-i;
			break;
		}
		if(!(*matrix)[i][j]) continue;
		const SDL_Rect r = {x*30 + 10 * mx, y*30 + 10 * my, 10, 10};
		SDL_RenderFillRect(ren, &r);
	}


}
void debug_mode(SDL_Renderer *ren, map *m, int debug)
{
	SDL_SetRenderDrawColor(ren, 211, 211, 211, 255);
	if (debug & 1) for(int i=0;i<WIDTH;++i) for(int j=0;j<HEIGHT;++j) {
		const SDL_Rect r = {i*30,j*30,30,30};
		SDL_RenderDrawRect(ren, &r);
	}
	SDL_SetRenderDrawColor(ren, 0, 255, 255, 255);
	if(debug & 2)  for(int i=0;i<WIDTH;++i) for(int j=0;j<HEIGHT;++j) {
		if(m->data[i][j]) continue;
		const int px1 = i * 30 + 15, py1 = j * 30 + 15; 
		for(int k=0;k<4;++k) {
			const int x = i + offset_neighbors[k].x, y = j + offset_neighbors[k].y;
			if((x < 0) || (x> WIDTH-1) || (y < 0) || (y > HEIGHT-1)) continue;
			if(m->data[x][y]) continue;
			const int px2 = x * 30 + 15, py2 = y * 30 + 15;
			SDL_RenderDrawLine(ren, px1, py1, px2, py2);
		}
	}

}

void render_pawn(SDL_Renderer *ren, pawn *p)
{
	for(int i=0;i<PAWNS_SIZE;++i){
		switch(i) {
			case PACMAN: SDL_SetRenderDrawColor(ren, 255, 255, 0, 255); break;
			case RED: SDL_SetRenderDrawColor(ren, 255, 0, 0, 255); break;
			case PINK: SDL_SetRenderDrawColor(ren, 255, 192, 203, 255); break;
			case CYAN: SDL_SetRenderDrawColor(ren, 224, 255, 255, 255); break;
			case ORANGE: SDL_SetRenderDrawColor(ren, 255, 165, 0, 255); break;
		}
		const SDL_Rect r = {p[i].x*30 - 5, p[i].y*30 - 5, 40, 40};
		SDL_RenderFillRect(ren, &r);
	}
}

void toggle_tile(map *m, int x, int y)
{
	//printf("Mouse button click with (%d, %d)\n", x, y);
	m->data[x/30][y/30] = !(m->data[x/30][y/30]);
	generate_graphic(m);

}

void handle_keypress(SDL_Keycode c, pawn *p, int *debug)
{
	switch(c) {
	case SDLK_F12: *debug = (*debug+1)%4; break;
	case SDLK_RIGHT: p->old_move = p->move;p->move = RIGHT;break;
	case SDLK_LEFT:  p->old_move = p->move;p->move = LEFT;break;
	case SDLK_UP:    p->old_move = p->move;p->move = UP;break;
	case SDLK_DOWN:  p->old_move = p->move;p->move = DOWN;break;
	}
}

void pawn_move(pawn *ps, map *m)
{
	if(ps[0].move == -1) return;
	for(pawn *p = ps; p < ps + PAWNS_SIZE; ++p) {
		if(p->move == -1) continue;
		int x = (p->x + offset_neighbors[p->move].x) % WIDTH, y = (p->y + offset_neighbors[p->move].y) % HEIGHT;
		if(x < 0) x = WIDTH - 1;
		if(y < 0) y = HEIGHT - 1;
		if(m->data[x][y]){
			if(p->old_move == -1) continue;
			x = (p->x + offset_neighbors[p->old_move].x) % WIDTH;
			y = (p->y + offset_neighbors[p->old_move].y) % HEIGHT;
			if(m->data[x][y]) continue;
		}	
		if((p->move == DOWN) && (y == HEIGHT/ 2 - 3) && ((x == WIDTH/2) || (x == (WIDTH/2 - 1)))) continue;
		p->x = x;
		p->y = y;
		p->old_move = -1;
	}

}

point pawn_ahead(pawn *p, map *m, int ratio)
{
	// TODO: walk around the maze instead.
	point self = {p->x, p->y};
	if(p->move == -1) return self;
	int x = p->x + ratio * offset_neighbors[p->move].x, y = p->y + ratio * offset_neighbors[p->move].y;

	while (((x < 0) || (x> WIDTH-1) || (y < 0) || (y > HEIGHT-1)) || (m->data[x][y])) {
		const int dx = abs(x - WIDTH / 2) / (x - WIDTH / 2);
		const int dy = abs(y - HEIGHT / 2) / (y - HEIGHT / 2);
		x-=dx;
		y-=dy;
	}
		
	printf("Returning (%d, %d)\n", x, y);
	point ret = {x, y};
	return ret;
}


int trace_route(int sx, int sy, int tx, int ty, map *m, int *out) 
{
	static point que[WIDTH * HEIGHT];
	static int hashmap[WIDTH * HEIGHT];

	if((sx == tx) && (sy == ty)) return -1;

	int head = 0; int tail = 0;
	for(int i=0;i<WIDTH*HEIGHT-1;++i) {
		hashmap[i] = -1;
	}
	que[tail].x = sx;
	que[tail].y = sy;
	++tail;


	int found = 0;
	while(!found) {
		assert(tail < WIDTH * HEIGHT);
		assert(tail - head);
		const point cur = que[head++];
		if((cur.x == tx) && (cur.y == ty)) {
			return (hashmap[cur.x * HEIGHT + cur.y] + 2) % 4;
		}
		for(int i=0;i<4;++i) {
			int x = (cur.x + offset_neighbors[i].x) % WIDTH, y = (cur.y + offset_neighbors[i].y) % HEIGHT;
			if(x < 0) x = WIDTH - 1;
			if(y < 0) y = HEIGHT - 1;
			if(m->data[x][y]) continue;
			if(hashmap[x * HEIGHT + y] >= 0) continue;
			hashmap[x * HEIGHT + y] = i;
			que[tail].x = x;
			que[tail].y = y;
			++tail;


		}
	}
}

int main(int argc, char *argv[])
{
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);
	SDL_Window *win = SDL_CreateWindow("neatMan", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1280, 1280, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);
	SDL_RenderSetLogicalSize(ren, 30 * WIDTH, 30 * HEIGHT);

	int debug = 0;
	unsigned int last_time=0, current_time;

	map m = {{0}};	
	pawn pawns[PAWNS_SIZE] = {{WIDTH / 2, 23, NONE, NONE},{WIDTH/2,HEIGHT/2 - 3, NONE, NONE},
				{WIDTH/2 + 1, HEIGHT/2 - 1, NONE, NONE},{WIDTH/2 - 1, HEIGHT/2 - 1, NONE, NONE},
				{WIDTH/2 - 3, HEIGHT/2 -1, NONE, NONE}};
	if(argc > 1) { 
		load_map(argv[1], &m);
		generate_graphic(&m);
	}

	for(int running = 1;running;) {
		SDL_Event e;
		while(SDL_PollEvent(&e)) {
			switch(e.type) {
			case SDL_QUIT: running = 0; break;
			case SDL_KEYDOWN: handle_keypress(e.key.keysym.sym, pawns, &debug);break;
			case SDL_MOUSEBUTTONDOWN: toggle_tile(&m, e.button.x, e.button.y);break;
			}
		}

		current_time = SDL_GetTicks();
		if(current_time - last_time >= 1000/8) {
			pawns[RED].move = trace_route(pawns[PACMAN].x, pawns[PACMAN].y, pawns[RED].x, pawns[RED].y, &m, NULL);
			const point pacman_ahead = pawn_ahead(&pawns[PACMAN], &m, 6);
			pawns[PINK].move = trace_route(pacman_ahead.x, pacman_ahead.y, pawns[PINK].x, pawns[PINK].y, &m, NULL);
			pawn_move(pawns, &m);
			last_time = current_time;
		}

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);

	
		
		SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
		for(int i=0;i<WIDTH;++i) for(int j=0;j<HEIGHT;++j) {
			if(!m.data[i][j]) continue;
			draw_tile(ren, &m.graphic[i][j], i, j);
		}

		render_pawn(ren, pawns);



		debug_mode(ren, &m, debug);
		SDL_RenderPresent(ren);
//		SDL_Delay(1000);
	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);

	save_map("out.txt", &m);

	return 0;
}
