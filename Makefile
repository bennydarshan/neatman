CC=c99
CFLAGS=-ggdb
CLIBS=-lSDL2

all: neatman

neatman: src/main.c
	$(CC) -o $@ $^ $(CFLAGS) $(CLIBS)

clean:
	rm neatman
